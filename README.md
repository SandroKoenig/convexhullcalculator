# ConvexHullCalculator

Simple console application for calculating the convex hull (https://en.wikipedia.org/wiki/Convex_hull) of a given set of coordinates (points) in the plane.