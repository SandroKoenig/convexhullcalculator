package convexhull.data;

/**
 * Implementing the data type point as representation of a point in the plane
 * with Integer coordinates. Point class implements the interface Comparable to
 * enable pairwise comparison.
 */
public class Point implements Comparable<Point> {

    private int x;
    private int y;

    /**
     * Constructor of Point class. Creates new Point with x and y coordinate.
     * 
     * @param x
     *            the x coordinate
     * @param y
     *            the y coordinate
     */
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Determine the location of a given point p to the direction vector from a
     * referenced point to another point q (this-q).
     * 
     * @param p
     *            the point whose position relative to the direction vector
     *            (this-q) is required
     * @param q
     *            the second point of the direction vector (this-q)
     * @return value whereby location of p relative to (this-q) is determined.
     * 
     *         <ul><li>(greater 0) p is left of (this-q)
     *         <li>(less 0) right of (this-q)
     *         <li>(result = 0) means collinearity.</ul>
     */
    public int leftOf(Point p, Point q) {
        int firstArg = (q.x - this.x) * (p.y - this.y);
        int secondArg = (p.x - this.x) * (q.y - this.y);
        return firstArg - secondArg;
    }

    /**
     * Calculating distance between two points, using the pythagorean theorem.
     * 
     * @param p
     *            the target point of the vector
     * @return the calculated distance between two points
     */
    public double dist(Point p) {
        double xDistance = p.x - this.x;
        double yDistance = p.y - this.y;
        return Math.sqrt((xDistance * xDistance) + (yDistance * yDistance));
    }

    /**
     * Checking if referenced point is equal to another.
     * 
     * @param o
     *            the reference object with which to compare.
     * @return
     *         <ul><li>true if points a equal
     *         <li>false if points are unequal, or given point is not an
     *         instance of point class.</ul>
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof Point) {
            Point other = (Point) o;
            return (this.compareTo(other) == 0);
        } else {
            return false;
        }
    }

    /**
     * Generating a String representation of a referenced point.
     * 
     * @return String representation of point in form of a tuple of coordinates
     */
    @Override
    public String toString() {
        return "(" + this.x + ", " + this.y + ")";
    }

    /**
     * Computing the hashCode of a given point.
     * 
     * @return the hashCode for the referenced point
     */
    @Override
    public int hashCode() {
        return 1000 * this.x + 50 * this.y;
    }

    /**
     * Comparing to points to each other according to x and y coordinates.
     * 
     * @param other
     *            the point with which to compare
     * 
     * @return
     *         <ul><li>(+1) if current point coordinates are bigger than of 
     *         other <li>(-1) if other point is bigger than current point
     *         <li>(0) if both points are equal </ul>
     */
    @Override
    public int compareTo(Point other) {
        if (compareCoordinates(this.x, other.x) == 0) {
            return compareCoordinates(this.y, other.y);
        } else {
            return compareCoordinates(this.x, other.x);
        }
    }

    /**
     * Comparing the coordinates of a given point to another.
     * 
     * @param given
     *            the x or y coordinate of the regarded point
     * @param other
     *            the x or y coordinate of the point with which to compare
     * @return
     *         <ul>
     *         <li>(+1) coordinate of given point is bigger than of the other.
     *         <li>(-1) coordinate of the other point is bigger.
     *         <li>(0) both points have same the same coordinate.</ul>
     */
    private int compareCoordinates(int given, int other) {
        if (given != other) {
            if (given > other) {
                return +1;
            } else {
                return -1;
            }
        } else {
            return 0;
        }
    }
}