package convexhull.data;

import java.util.Comparator;

/**
 * AngleDistanceComparator class functions as helper class for eliminating
 * collinear points in a field. Implements the interface Comparator for
 * comparing two points by their angle-distance relative to a given point.
 */
public class AngleDistComparator implements Comparator<Point> {

    private Point reference;

    /**
     * Constructor of AngleDistanceComparator class.
     * 
     * @param ref
     *            the referenced point for the comparison
     */
    public AngleDistComparator(Point ref) {
        this.reference = ref;
    }

    /**
     * Comparing to points by their angle distance to a referenced point. Means
     * comparing the angle of two points to a reference point. If collinearity
     * occurs, the distance of point nearer to the reference point is chosen.
     * 
     * @param p
     *            the point whose angle distance relative to direction vector
     *            (this-q) is required
     * @param q
     *            the second point of the direction vector (this-q)
     * 
     * @return
     *         <ul><li>in case of collinearity: the angle distance to the point
     *         which is nearer to reference point.
     *         <li>otherwise: the angle distance to point p.</ul>
     */
    public int compare(Point p, Point q) {
        int c = reference.leftOf(p, q);
        if (c == 0) {
            return (int) Math.signum(reference.dist(p) - reference.dist(q));
        } else {
            return c;
        }
    }
}