package convexhull.data;

import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.List;
import java.util.StringJoiner;

/**
 * Implementation of a Field, able to contain any amount of points. Points can
 * be added or removed to a field. A field can compute the convex hull of the
 * amount of points the field is containing.
 */
public class Field {

    private List<Point> points;

    /**
     * Constructor of Field class. Creates a new, empty Field.
     */
    public Field() {
        this.points = new LinkedList<Point>();
    }

    /**
     * Adding a given point to a field, when a point with the same coordinates
     * does not already exist in the field.
     * 
     * @param point
     *            the point to add
     * @return true if point was added successful - false otherwise
     */
    public boolean addPoint(Point point) {
        if (points.contains(point)) {
            return false;
        } else {
            return points.add(point);
        }
    }

    /**
     * Removing a given Point from a field, if it is existent in the field.
     * 
     * @param point
     *            the point to remove
     * @return true if removing was successful - false otherwise
     */
    public boolean removePoint(Point point) {
        if (points.contains(point)) {
            return points.remove(point);
        } else {
            return false;
        }
    }

    /**
     * Computing the convex hull of an amount of points of a field. Starting by
     * removing all collinear points from the field. Afterwards checking if
     * there are less than 3 points remaining. If so, special case treatment
     * ensues. Otherwise the convex hull is computed by use of the graham scan
     * algorithm.
     * 
     * @return the convex hull in the right order
     */
    public List<Point> convexHull() {
        if (this.points.isEmpty()) {
            return this.points;
        } else {
            Point p0 = Collections.min(this.points);
            List<Point> ordPoints = this.wipeCollinears(p0, this.points);

            if (ordPoints.size() < 3) {
                return specialCases(ordPoints, p0);
            } else {
                List<Point> result 
                        = ((LinkedList<Point>) grahamScan(ordPoints, p0));
                Collections.reverse((LinkedList<Point>) result);
                return result;
            }
        }
    }

    /**
     * Implementing the graham scan algorithm for computation of the convex
     * hull. Starting off by a stack containing 3 points. Initially the minimum
     * point (most left, lowest), and the two following points in the amount of
     * points in a field free of collinear points.
     * 
     * @param ordPoints
     *            the given list of points - wiped out of collinear points
     * @param min
     *            the minimal (most left,lowest) point of a field
     * @return the convex hull of a field in reversed order
     */
    private Deque<Point> grahamScan(List<Point> ordPoints, Point min) {
        Deque<Point> stack = new LinkedList<Point>();
        int i = 2;
        stack.push(min);
        stack.push(ordPoints.get(0));
        if (ordPoints.size() > 1) {
            stack.push(ordPoints.get(1));
        }
        while (i < ordPoints.size()) {
            if (getPrev(stack).leftOf(ordPoints.get(i), stack.peek()) <= 0) {
                stack.pop();
            } else {
                stack.push(ordPoints.get(i));
                i++;
            }
        }
        return stack;
    }

    /**
     * Getting previous element ( = below) of top element from a stack.
     * 
     * @param stack
     *            the regarded stack
     * @return the previous element which was requested
     */
    private Point getPrev(Deque<Point> stack) {
        Point top = stack.pop();
        Point requested = stack.peek();
        stack.push(top);
        return requested;
    }

    /**
     * Wiping all collinear points out of a given list. Therefore sorting the
     * list by the angle distance relative to the given initial point, with aid
     * of an AngleDistComparator.
     * 
     * @param min
     *            the minimal point of a field (most left,lowest)
     * @param points
     *            the initial list of points - unsorted (and perhaps with
     *            collinear points).
     * @return a new list of points, wiped out of all collinear points and
     *         sorted by angle distance relative to the minimal point.
     */
    private List<Point> wipeCollinears(Point min, List<Point> points) {
        if (points.size() > 1) {
            List<Point> result = new LinkedList<Point>();
            List<Point> temp = new ArrayList<Point>(points);
            Collections.sort(temp, new AngleDistComparator(min));

            for (int i = 0; i < temp.size() - 1; i++) {
                if (min.leftOf(temp.get(i), temp.get(i + 1)) != 0) {
                    result.add(temp.get(i));
                }
            }
            result.add(temp.get(temp.size() - 1));
            return result;
        } else {
            return points;
        }
    }

    /**
     * Treating special cases occurring by computation of convex hull. Special
     * cases emerge, if field (without collinears) contains less than 3 points.
     * 
     * @param ordPoints
     *            the given list of points - wiped out of collinear points
     * @param min
     *            the minimal point of the field (most left,lowest)
     * @return the convex hull of a special case field
     */
    private List<Point> specialCases(List<Point> ordPoints, Point min) {
        if (ordPoints.get(0).equals(min)) {
            return ordPoints;
        } else {
            ordPoints.add(0, min);
            return ordPoints;
        }
    }

    /**
     * Generating a String representation for each point in a field.
     * 
     * @return String representation of a referenced field
     */
    @Override
    public String toString() {
        StringJoiner sj = new StringJoiner(", ", "[", "]");
        for (Point point : this.points) {
            sj.add(point.toString());
        }
        return sj.toString();
    }

    /**
     * Printing referenced field sorted in ascending order of point coordinates.
     * 
     * @return String representation of a sorted field
     */
    public String printField() {
        Collections.sort(this.points);
        return this.toString();
    }
}