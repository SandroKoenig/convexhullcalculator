package convexhull.shell;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import convexhull.data.Field;
import convexhull.data.Point;

/**
 * Shell class handles all user interactions with the program, which are entered
 * on the command line after starting the program.
 */
public final class Shell {
    
   /**
    * Utility class implies private constructor.
    */
    private Shell() {
    }

    /**
     * Entry point of the program. Creates a BufferedReader to read user inputs
     * executes the treatment of user inputs by calling the execute(); method.
     * 
     * @param args
     *            array of command line arguments
     * @throws IOException
     *             might throw an IOException regardless of user interaction
     */
    public static void main(String[] args) throws IOException {
        BufferedReader stdin 
                = new BufferedReader(new InputStreamReader(System.in));
        execute(stdin);
    }

    /**
     * Treating all entered commands and arguments by the user, and execute a
     * case differentiation by the token/command entered at first. A new Field
     * is already created by the start of the program.
     * 
     * @param stdin
     *            BufferedReader for reading user interactions from the command
     *            line
     * @throws IOException
     *             might throw an IOException regardless of user interaction
     */
    public static void execute(BufferedReader stdin) throws IOException {
        Field field = new Field();
        boolean quit = false;

        while (!quit) {
            System.out.print("ch> ");
            String input = stdin.readLine().toLowerCase();
            if (input == null) {
                break;
            }
            // split input on white spaces
            String[] tokens = input.trim().split("\\s+");
            if (tokens[0].equals("")) {
                continue;
            }
            switch (tokens[0].charAt(0)) {

            case 'n':
                // NEW
                field = null;
                Field temp = new Field();
                field = temp;
                break;
            case 'a':
            case 'r':
                // ADD or REMOVE
                tryModification(tokens, field);
                break;
            case 'p':
                // PRINT
                System.out.println(field.printField());
                break;
            case 'c':
                // CONVEXHULL
                System.out.println(field.convexHull());
                break;
            // HELP
            case 'h':
                System.out.println(help());
                break;
            // QUIT
            case 'q':
                quit = true;
                stdin.close();
                break;
            default:
                // Any other command is unknown
                System.out.println("Error! Unknown command!");
            }
        }
    }

    /**
     * Trying to modify (adding or removing points) the field according to
     * entered tokens. Case differentiation between adding or removing. An error
     * occurs if all, or one of the entered tokens is not valid.
     * 
     * @param tokens
     *            all tokens entered in the command line
     * @param field
     *            the field to be affected by the modification
     */
    private static void tryModification(String[] tokens, Field field) {
        try {
            if (tokens.length < 3) {
                System.out.println("Error! Missing one or more coordinates!");
            } else {
                int xCoord = Integer.parseInt(tokens[1]);
                int yCoord = Integer.parseInt(tokens[2]);
                if (tokens[0].charAt(0) == 'a') {
                    performAddition(xCoord, yCoord, field);
                } else {
                    performRemoval(xCoord, yCoord, field);
                }
            }
        } catch (NumberFormatException exc) {
            System.out.println("Error! One of the coordinates " 
                    + "isn't a valid number");
        }
    }

    /**
     * Entered point is added to the field,if not already present in the field.
     * 
     * @param xCoord
     *            the x coordinate for the Point to add
     * @param yCoord
     *            the y coordinate for the Point to add
     * @param field
     *            the field to which the point gets added
     */
    private static void performAddition(int xCoord, int yCoord, Field field) {
        Point toAdd = new Point(xCoord, yCoord);
        if (!field.addPoint(toAdd)) {
            System.out.println("Error! Point already in list!");
        }
    }

    /**
     * Entered point is removed from the field, if the field actually contains
     * the point.
     * 
     * @param xCoord
     *            the x coordinate for the Point to remove
     * @param yCoord
     *            the y coordinate for the Point to remove
     * @param field
     *            the field from which the point gets removed
     */
    private static void performRemoval(int xCoord, int yCoord, Field field) {
        Point toRemove = new Point(xCoord, yCoord);
        if (!field.removePoint(toRemove)) {
            System.out.println("Error! Point is not in list!");
        }
    }

    /**
     * Listing a help text with all available commands by the prompt.
     * 
     * @return the help text as a String
     */
    private static String help() {
        StringBuilder sb = new StringBuilder();
        sb.append("Commands available from the prompt:\n\n");
        sb.append("NEW        : Initializing a new Field, and deleting the");
        sb.append(" old one\n");
        sb.append("ADD    x y : Adding a new Point to the Field - Error ");
        sb.append("if Point already exists\n");
        sb.append("REMOVE x y : Removing given Point from the Field -");
        sb.append(" Error if Point isn't in the Field\n");
        sb.append("CONVEX     : Calculating and printing the convex hull of ");
        sb.append("the given Field \n");
        sb.append("PRINT      : Printing field sorted by x and y ");
        sb.append("coordinates in ascending order\n");
        sb.append("HELP       : Printing help text for an overview of all ");
        sb.append("existing commands\n");
        sb.append("QUIT       : Terminating the program\n");
        return sb.toString();
    }
}